<?php
namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class ParkingSpot
 *
 * @package App
 * @property string $name
 * @property string $current_car
 * @property string $parent
*/
class ParkingSpot extends Model
{
    use SoftDeletes;


    protected $fillable = ['name', 'current_car_id', 'parent_id', 'seconds_used'];


    public static function storeValidation($request)
    {
        return [
            'name' => 'min:1|max:10|required',
            'current_car_id' => 'integer|exists:cars,id|max:4294967295|nullable',
            'parent_id' => 'integer|exists:parking_spots,id|max:4294967295|nullable'
        ];
    }

    public static function updateValidation($request)
    {
        return [
            'name' => 'min:1|max:10|required',
            'current_car_id' => 'integer|exists:cars,id|max:4294967295|nullable',
            'parent_id' => 'integer|exists:parking_spots,id|max:4294967295|nullable'
        ];
    }





    public function current_car()
    {
        return $this->belongsTo(Car::class, 'current_car_id')->withTrashed();
    }

    public function parent()
    {
        return $this->belongsTo(ParkingSpot::class, 'parent_id')->withTrashed();
    }


}
