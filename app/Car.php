<?php
namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Car
 *
 * @package App
 * @property string $license_plate
 * @property string $model
*/
class Car extends Model
{
    use SoftDeletes;


    protected $fillable = ['license_plate', 'model','alegra_id'];


    public static function storeValidation($request)
    {
        return [
            'license_plate' => 'min:1|max:191|required',
            'model' => 'min:1|max:191|required'
        ];
    }

    public static function updateValidation($request)
    {
        return [
            'license_plate' => 'min:1|max:191|required',
            'model' => 'min:1|max:191|required'
        ];
    }






}
