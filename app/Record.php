<?php
namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Record
 *
 * @package App
 * @property string $parking
 * @property string $car
*/
class Record extends Model
{
    use SoftDeletes;


    protected $fillable = ['parking_id', 'car_id', 'parking_id_out', 'final_cost', 'seconds','alegra_id'];


    public static function storeValidation($request)
    {
        return [
            'parking_id' => 'integer|exists:parking_spots,id|max:4294967295|required',
            'car_id' => 'integer|exists:cars,id|max:4294967295|required'
        ];
    }

    public static function updateValidation($request)
    {
        return [
            'parking_id' => 'integer|exists:parking_spots,id|max:4294967295|required',
            'car_id' => 'integer|exists:cars,id|max:4294967295|required'
        ];
    }





    public function parking()
    {
        return $this->belongsTo(ParkingSpot::class, 'parking_id')->withTrashed();
    }

    public function parkingOut()
    {
        return $this->belongsTo(ParkingSpot::class, 'parking_id_out')->withTrashed();
    }

    public function car()
    {
        return $this->belongsTo(Car::class, 'car_id')->withTrashed();
    }


}
