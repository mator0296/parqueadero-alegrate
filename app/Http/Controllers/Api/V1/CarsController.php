<?php

namespace App\Http\Controllers\Api\V1;

use App\Car;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Http\Resources\Car as CarResource;
use App\Http\Requests\Admin\StoreCarsRequest;
use App\Http\Requests\Admin\UpdateCarsRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;



class CarsController extends Controller
{
    public function index()
    {


        return new CarResource(Car::with([])->get());
    }

    public function CarsWhitout()
    {

        return new CarResource(Car::whereNotExists(function ($query) {
                $query->select(DB::raw('*'))
                      ->from('parking_spots')
                      ->whereRaw('parking_spots.current_car_id = cars.id');
            })
            ->get());
    }

    public function show($id)
    {
        if (Gate::denies('car_view')) {
            return abort(401);
        }

        $car = Car::with([])->findOrFail($id);

        return new CarResource($car);
    }

    public function store(StoreCarsRequest $request)
    {
        if (Gate::denies('car_create')) {
            return abort(401);
        }

        $car = Car::create($request->all());



        return (new CarResource($car))
            ->response()
            ->setStatusCode(201);
    }

    public function update(UpdateCarsRequest $request, $id)
    {
        if (Gate::denies('car_edit')) {
            return abort(401);
        }

        $car = Car::findOrFail($id);
        $car->update($request->all());




        return (new CarResource($car))
            ->response()
            ->setStatusCode(202);
    }

    public function destroy($id)
    {
        if (Gate::denies('car_delete')) {
            return abort(401);
        }

        $car = Car::findOrFail($id);
        $car->delete();

        return response(null, 204);
    }
}
