<?php

namespace App\Http\Controllers\Api\V1;

use App\Record;
use App\Http\Controllers\Controller;
use App\Http\Resources\Record as RecordResource;
use App\Http\Requests\Admin\StoreRecordsRequest;
use App\Http\Requests\Admin\UpdateRecordsRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;



class RecordsController extends Controller
{
    public function index()
    {


        return new RecordResource(Record::withTrashed()->whereNotNull('deleted_at')->with(['parking', 'car','parkingOut'])->get());
    }

    public function MostCarsTime()
    {

        return new RecordResource(Record::groupBy('car_id')->orderByRaw('sum(seconds) DESC')->selectRaw('car_id, sum(seconds) as totalSeconds')->withTrashed()->with(['parking', 'car'])->get());
    }

    public function show($id)
    {
        if (Gate::denies('record_view')) {
            return abort(401);
        }

        $record = Record::withTrashed()->with(['parking', 'car','parkingOut'])->findOrFail($id);

        return new RecordResource($record);
    }

    public function store(StoreRecordsRequest $request)
    {
        if (Gate::denies('record_create')) {
            return abort(401);
        }

        $record = Record::create($request->all());



        return (new RecordResource($record))
            ->response()
            ->setStatusCode(201);
    }

    public function update(UpdateRecordsRequest $request, $id)
    {
        if (Gate::denies('record_edit')) {
            return abort(401);
        }

        $record = Record::withTrashed()->findOrFail($id);
        $record->update($request->all());




        return (new RecordResource($record))
            ->response()
            ->setStatusCode(202);
    }

    public function destroy($id)
    {
        if (Gate::denies('record_delete')) {
            return abort(401);
        }

        $record = Record::withTrashed()->findOrFail($id);
        $record->forceDelete();

        return response(null, 204);
    }

    public function sendForEmail(StoreRecordsRequest $request, $id)
    {
        $fields = array(
            'emails' => $request->email,
            'invoiceType' =>'copy'
		);
        //return $fields;
		$fields = json_encode($fields);

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, "https://app.alegra.com/api/v1/invoices/".$id."/email");
		curl_setopt($ch, CURLOPT_HTTPHEADER, array(
		    'Content-Type: application/json; charset=utf-8',
		    'Authorization: Basic '.base64_encode(env('EMAILALEGRA').':'.env('TOKENALEGRA'))
		));
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
		curl_setopt($ch, CURLOPT_HEADER, FALSE);
		curl_setopt($ch, CURLOPT_POST, TRUE);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);

		$response = curl_exec($ch);
		curl_close($ch);

		return (array)json_decode($response);
    }
}
