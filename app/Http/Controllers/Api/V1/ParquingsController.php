<?php

namespace App\Http\Controllers\Api\V1;

use App\Parquing;
use App\Http\Controllers\Controller;
use App\Http\Resources\Parquing as ParquingResource;
use App\Http\Requests\Admin\StoreParquingsRequest;
use App\Http\Requests\Admin\UpdateParquingsRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;



class ParquingsController extends Controller
{
    public function index()
    {
        return new ParquingResource(Parquing::all());
    }
}
