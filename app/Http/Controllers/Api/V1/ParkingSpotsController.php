<?php

namespace App\Http\Controllers\Api\V1;

use App\ParkingSpot;
use App\Car;
use App\Record;
use App\Http\Controllers\Controller;
use App\Http\Resources\ParkingSpot as ParkingSpotResource;
use App\Http\Requests\Admin\StoreParkingSpotsRequest;
use App\Http\Requests\Admin\UpdateParkingSpotsRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use Carbon\Carbon;


class ParkingSpotsController extends Controller
{
    public function index()
    {


        return new ParkingSpotResource(ParkingSpot::with(['current_car', 'parent'])->get());
    }

    public function show($id)
    {
        if (Gate::denies('parking_spot_view')) {
            return abort(401);
        }

        $parking_spot = ParkingSpot::with(['current_car', 'parent'])->findOrFail($id);

        return new ParkingSpotResource($parking_spot);
    }

    public function store(StoreParkingSpotsRequest $request)
    {
        if (Gate::denies('parking_spot_create')) {
            return abort(401);
        }

        $parking_spot = ParkingSpot::create($request->all());



        return (new ParkingSpotResource($parking_spot))
            ->response()
            ->setStatusCode(201);
    }

    public function calculateSpot()
    {
        $freeSpot = ParkingSpot::with('parent')->whereNull('current_car_id')->get();

        if (!count($freeSpot)) {
            return null;
        }
        $freeSpot = $freeSpot->random(1)[0];
        if ($freeSpot->parent != null)
            if ($freeSpot->parent->current_car_id == null) {
                $freeSpot = $freeSpot->parent;
            }
        return $freeSpot;

    }

    public function update(UpdateParkingSpotsRequest $request, $id)
    {
        if (Gate::denies('parking_spot_edit')) {
            return abort(401);
        }

        $parking_spot = ParkingSpot::findOrFail($id);
        $parking_spot->update($request->all());

        return (new ParkingSpotResource($parking_spot))
            ->response()
            ->setStatusCode(202);
    }

    public function destroy($id)
    {
        if (Gate::denies('parking_spot_delete')) {
            return abort(401);
        }

        $parking_spot = ParkingSpot::findOrFail($id);
        $parking_spot->delete();

        return response(null, 204);
    }

    public function checkoutData($id)
    {
        if (Gate::denies('parking_spot_view')) {
            return abort(401);
        }
        $parking_spot = ParkingSpot::with(['current_car', 'parent'])->findOrFail($id);
        $childSpot = ParkingSpot::with(['current_car', 'parent'])->where('parent_id', $id)->first();
        $newSpot = $this->calculateSpot();


        return [
                'spot' => new ParkingSpotResource($parking_spot),
                'child' => new ParkingSpotResource($childSpot),
                'newSpot' => new ParkingSpotResource($newSpot),
            ];
    }

    public function registerInvoice($record, $seconds) {
        $car = Car::findOrFail($record->car_id);
		$fields = array(
            "date"=> Carbon::parse($record->deleted_at)->format('Y-m-d'),
            "dueDate"=> Carbon::parse($record->deleted_at)->format('Y-m-d'),
            "client"=>  $car->alegra_id,
            "status" => 'open',
            "items" => [
                [
                  "id"=> 1,
                  "price" => 30,
                  "quantity" => $seconds
                ]
            ]
		);
        //return $fields;
		$fields = json_encode($fields);

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, "https://app.alegra.com/api/v1/invoices");
		curl_setopt($ch, CURLOPT_HTTPHEADER, array(
		    'Content-Type: application/json; charset=utf-8',
		    'Authorization: Basic '.base64_encode(env('EMAILALEGRA').':'.env('TOKENALEGRA'))
		));
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
		curl_setopt($ch, CURLOPT_HEADER, FALSE);
		curl_setopt($ch, CURLOPT_POST, TRUE);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);

		$response = curl_exec($ch);
		curl_close($ch);

		return ((array)json_decode($response))['id'];
	}


    public function calculateTime($spot)
    {
        $fechaEntrada = Carbon::parse($spot->updated_at);
        $today = Carbon::now();

        return $today->diffInSeconds($fechaEntrada);
    }

    public function sendForEmail($id, $email)
    {
        $fields = array(
            'emails' => $email,
            'invoiceType' =>'copy'
		);
        //return $fields;
		$fields = json_encode($fields);

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, "https://app.alegra.com/api/v1/invoices/".$id."/email");
		curl_setopt($ch, CURLOPT_HTTPHEADER, array(
		    'Content-Type: application/json; charset=utf-8',
		    'Authorization: Basic '.base64_encode(env('EMAILALEGRA').':'.env('TOKENALEGRA'))
		));
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
		curl_setopt($ch, CURLOPT_HEADER, FALSE);
		curl_setopt($ch, CURLOPT_POST, TRUE);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);

		$response = curl_exec($ch);
		curl_close($ch);

		return (array)json_decode($response);
    }

    public function registerClientAlegra($licensePlate)
    {
        $fields = array(
            "name"=> $licensePlate,

		);
        //return $fields;
		$fields = json_encode($fields);

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, "https://app.alegra.com/api/v1/contacts");
		curl_setopt($ch, CURLOPT_HTTPHEADER, array(
		    'Content-Type: application/json; charset=utf-8',
		    'Authorization: Basic '.base64_encode(env('EMAILALEGRA').':'.env('TOKENALEGRA'))
		));
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
		curl_setopt($ch, CURLOPT_HEADER, FALSE);
		curl_setopt($ch, CURLOPT_POST, TRUE);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);

		$response = curl_exec($ch);
		curl_close($ch);
        return (array)json_decode($response);
    }
    public function checkout(UpdateParkingSpotsRequest $request)
    {

        if (Gate::denies('parking_spot_edit')) {
            return abort(401);
        }

        $parking_spot = ParkingSpot::findOrFail($request->id);
        $childSpot = ParkingSpot::where('parent_id', $request->id)->first();
        $newSpot = ParkingSpot::where('parent_id', $request->newSpot_id)->first();
        $record = Record::where('car_id', $parking_spot->current_car_id)->first();



        if ($childSpot) {
            if ($childSpot->current_car_id) {
                if (!$newSpot) {
                    $parking_spot->update(['current_car_id'=>$childSpot->current_car_id]);
                    $childSpot->update(['current_car_id'=>null, 'seconds_used'=> $childSpot->seconds_used + $this->calculateTime($childSpot)]);
                    $record->update(['alegra_id'=>$this->registerInvoice($record, $request->seconds),'final_cost'=> $request->cost, 'seconds'=>$request->seconds, 'parking_id_out'=> $request->id]);
                    if ($request->has('email'))
                        $this->sendForEmail($record->alegra_id, $request->email);
                    $record->delete();
                    return (new ParkingSpotResource($parking_spot))
                        ->response()
                        ->setStatusCode(202);

                }
                $newSpot->update(['current_car_id'=>$childSpot->current_car_id]);
                $childSpot->update(['current_car_id'=>null, 'seconds_used'=> $childSpot->seconds_used + $this->calculateTime($childSpot)]);
            }

        }

        //return $parking_spot->seconds_used + $this->calculateTime($parking_spot);
        $parking_spot->update(['seconds_used'=> ($parking_spot->seconds_used + $this->calculateTime($parking_spot)),'current_car_id'=>null]);
        $record->update(['alegra_id'=>$this->registerInvoice($record, $request->seconds),'final_cost'=> $request->cost, 'seconds'=>$request->seconds, 'parking_id_out'=> $request->id]);
        if ($request->has('email'))
            $this->sendForEmail($record->alegra_id, $request->email);
        $record->delete();


        return (new ParkingSpotResource($parking_spot))
            ->response()
            ->setStatusCode(202);
    }

    public function registerCarSpot(UpdateParkingSpotsRequest $request, $id)
    {
        if (Gate::denies('parking_spot_edit')) {
            return abort(401);
        }

        $parking_spot = ParkingSpot::findOrFail($id);

        if ($request->newCar) {
            $car = Car::where('license_plate', $request->license_plate)->first();
            if (!$car){
                $clientCar = $this->registerClientAlegra($request->license_plate);
                //return $clientCar;
                $car = Car::create(['license_plate'=>$request->license_plate, 'model'=>$request->model,'alegra_id'=> $clientCar['id']]);

            }

            $parking_spot->update(['name'=>$request->name,'current_car_id'=>$car->id]);
            Record::create(['parking_id'=> $parking_spot->id, 'car_id'=> $car->id]);

        } else {

            $parking_spot->update(['name'=>$request->name,'current_car_id'=>$request->current_car_id]);
            Record::create(['parking_id'=> $parking_spot->id, 'car_id'=> $request->current_car_id]);

        }

        return (new ParkingSpotResource($parking_spot))
            ->response()
            ->setStatusCode(202);
    }

    public function MostTime()
    {
        return new ParkingSpotResource(ParkingSpot::orderBy('seconds_used','desc')->limit(5)->with(['current_car', 'parent'])->get());
    }



}
