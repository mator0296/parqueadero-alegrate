<?php

Route::group(['prefix' => '/v1', 'middleware' => ['auth:api'], 'namespace' => 'Api\V1', 'as' => 'api.'], function () {
    Route::post('change-password', 'ChangePasswordController@changePassword')->name('auth.change_password');
    Route::get('parking-spots/checkoutdata/{id}','ParkingSpotsController@checkoutData');
    Route::put('parking-spots/register-car-spot/{id}','ParkingSpotsController@registerCarSpot');
    Route::put('parking-spots/checkout/{id}','ParkingSpotsController@checkout');
    Route::get('parking-spots/mosttime','ParkingSpotsController@MostTime');
    Route::get('cars/free','CarsController@CarsWhitout');
    Route::get('records/mostcartime','RecordsController@MostCarsTime');
    Route::post('records/sendemail/{id}','RecordsController@sendForEmail');
    Route::apiResource('rules', 'RulesController', ['only' => ['index']]);
    Route::apiResource('users', 'UsersController');
    Route::apiResource('roles', 'RolesController');
    Route::apiResource('permissions', 'PermissionsController');
    Route::apiResource('parking-spots', 'ParkingSpotsController');
    Route::apiResource('cars', 'CarsController');
    Route::apiResource('records', 'RecordsController');


});
