import Vue from 'vue'
import VueRouter from 'vue-router'

import ChangePassword from '../components/ChangePassword.vue'
import UsersIndex from '../components/cruds/Users/Index.vue'
import UsersCreate from '../components/cruds/Users/Create.vue'
import UsersShow from '../components/cruds/Users/Show.vue'
import UsersEdit from '../components/cruds/Users/Edit.vue'
import RolesIndex from '../components/cruds/Roles/Index.vue'
import RolesCreate from '../components/cruds/Roles/Create.vue'
import RolesShow from '../components/cruds/Roles/Show.vue'
import RolesEdit from '../components/cruds/Roles/Edit.vue'
import PermissionsIndex from '../components/cruds/Permissions/Index.vue'
import PermissionsCreate from '../components/cruds/Permissions/Create.vue'
import PermissionsShow from '../components/cruds/Permissions/Show.vue'
import PermissionsEdit from '../components/cruds/Permissions/Edit.vue'
import ParkingSpotsIndex from '../components/cruds/ParkingSpots/Index.vue'
import ParkingSpotsCreate from '../components/cruds/ParkingSpots/Create.vue'
import ParkingSpotsShow from '../components/cruds/ParkingSpots/Show.vue'
import ParkingSpotsCheckOut from '../components/cruds/ParkingSpots/CheckOut.vue'
import ParkingSpotsEdit from '../components/cruds/ParkingSpots/Edit.vue'
import ParkingSpotsEnterCar from '../components/cruds/ParkingSpots/EditEnterCar.vue'
import ParkingSpotsGeneralState from '../components/cruds/ParkingSpots/GeneralState.vue'
import CarsIndex from '../components/cruds/Cars/Index.vue'
import CarsCreate from '../components/cruds/Cars/Create.vue'
import CarsShow from '../components/cruds/Cars/Show.vue'
import CarsEdit from '../components/cruds/Cars/Edit.vue'
import RecordsMostCar from '../components/cruds/Records/MostCar.vue'
import RecordsMostSpot from '../components/cruds/Records/MostSpot.vue'
import RecordsIndex from '../components/cruds/Records/Index.vue'
import RecordsCreate from '../components/cruds/Records/Create.vue'
import RecordsShow from '../components/cruds/Records/Show.vue'
import RecordsEdit from '../components/cruds/Records/Edit.vue'

Vue.use(VueRouter)

const routes = [
    { path: '/change-password', component: ChangePassword, name: 'auth.change_password' },
    { path: '/users', component: UsersIndex, name: 'users.index' },
    { path: '/users/create', component: UsersCreate, name: 'users.create' },
    { path: '/users/:id', component: UsersShow, name: 'users.show' },
    { path: '/users/:id/edit', component: UsersEdit, name: 'users.edit' },
    { path: '/roles', component: RolesIndex, name: 'roles.index' },
    { path: '/roles/create', component: RolesCreate, name: 'roles.create' },
    { path: '/roles/:id', component: RolesShow, name: 'roles.show' },
    { path: '/roles/:id/edit', component: RolesEdit, name: 'roles.edit' },
    { path: '/permissions', component: PermissionsIndex, name: 'permissions.index' },
    { path: '/permissions/create', component: PermissionsCreate, name: 'permissions.create' },
    { path: '/permissions/:id', component: PermissionsShow, name: 'permissions.show' },
    { path: '/permissions/:id/edit', component: PermissionsEdit, name: 'permissions.edit' },
    { path: '/parking-spots', component: ParkingSpotsIndex, name: 'parking_spots.index' },
    { path: '/parking-spots-general', component: ParkingSpotsGeneralState, name: 'parking_spots.general' },
    { path: '/parking-spots/create', component: ParkingSpotsCreate, name: 'parking_spots.create' },
    { path: '/parking-spots/:id', component: ParkingSpotsShow, name: 'parking_spots.show' },
    { path: '/parking-spots/:id/checkout', component: ParkingSpotsCheckOut, name: 'parking_spots.checkout' },
    { path: '/parking-spots/:id/edit', component: ParkingSpotsEdit, name: 'parking_spots.edit' },
    { path: '/parking-spots/:id/enter-car', component: ParkingSpotsEnterCar, name: 'parking_spots.enter_car' },
    { path: '/cars', component: CarsIndex, name: 'cars.index' },
    { path: '/cars/create', component: CarsCreate, name: 'cars.create' },
    { path: '/cars/:id', component: CarsShow, name: 'cars.show' },
    { path: '/cars/:id/edit', component: CarsEdit, name: 'cars.edit' },
    { path: '/records', component: RecordsIndex, name: 'records.index' },
    { path: '/records/mostcars', component: RecordsMostCar, name: 'records.mostcar' },
    { path: '/records/mostspots', component: RecordsMostSpot, name: 'records.mostspot' },
    { path: '/records/create', component: RecordsCreate, name: 'records.create' },
    { path: '/records/:id', component: RecordsShow, name: 'records.show' },
    { path: '/records/:id/edit', component: RecordsEdit, name: 'records.edit' },
]

export default new VueRouter({
    mode: 'history',
    base: '/admin',
    routes
})
