function initialState() {
    return {
        item: {
            id: null,
            parking: null,
            car: null,
            parkingOut:null,
        },
        parkingspotsAll: [],
        carsAll: [],
        email:'',
        loading: false,
    }
}

const getters = {
    item: state => state.item,
    loading: state => state.loading,
    parkingspotsAll: state => state.parkingspotsAll,
    carsAll: state => state.carsAll,
    email: state =>state.email,

}

const actions = {
    storeData({ commit, state, dispatch }) {
        commit('setLoading', true)
        dispatch('Alert/resetState', null, { root: true })

        return new Promise((resolve, reject) => {
            let params = new FormData();

            for (let fieldName in state.item) {
                let fieldValue = state.item[fieldName];
                if (typeof fieldValue !== 'object') {
                    params.set(fieldName, fieldValue);
                } else {
                    if (fieldValue && typeof fieldValue[0] !== 'object') {
                        params.set(fieldName, fieldValue);
                    } else {
                        for (let index in fieldValue) {
                            params.set(fieldName + '[' + index + ']', fieldValue[index]);
                        }
                    }
                }
            }

            if (_.isEmpty(state.item.parking)) {
                params.set('parking_id', '')
            } else {
                params.set('parking_id', state.item.parking.id)
            }
            if (_.isEmpty(state.item.car)) {
                params.set('car_id', '')
            } else {
                params.set('car_id', state.item.car.id)
            }

            axios.post('/api/v1/records', params)
                .then(response => {
                    commit('resetState')
                    resolve()
                })
                .catch(error => {
                    let message = error.response.data.message || error.message
                    let errors  = error.response.data.errors

                    dispatch(
                        'Alert/setAlert',
                        { message: message, errors: errors, color: 'danger' },
                        { root: true })

                    reject(error)
                })
                .finally(() => {
                    commit('setLoading', false)
                })
        })
    },
    updateData({ commit, state, dispatch }) {
        commit('setLoading', true)
        dispatch('Alert/resetState', null, { root: true })

        return new Promise((resolve, reject) => {
            let params = new FormData();
            params.set('_method', 'PUT')

            for (let fieldName in state.item) {
                let fieldValue = state.item[fieldName];
                if (typeof fieldValue !== 'object') {
                    params.set(fieldName, fieldValue);
                } else {
                    if (fieldValue && typeof fieldValue[0] !== 'object') {
                        params.set(fieldName, fieldValue);
                    } else {
                        for (let index in fieldValue) {
                            params.set(fieldName + '[' + index + ']', fieldValue[index]);
                        }
                    }
                }
            }

            if (_.isEmpty(state.item.parking)) {
                params.set('parking_id', '')
            } else {
                params.set('parking_id', state.item.parking.id)
            }
            if (_.isEmpty(state.item.car)) {
                params.set('car_id', '')
            } else {
                params.set('car_id', state.item.car.id)
            }

            axios.post('/api/v1/records/' + state.item.id, params)
                .then(response => {
                    commit('setItem', response.data.data)
                    resolve()
                })
                .catch(error => {
                    let message = error.response.data.message || error.message
                    let errors  = error.response.data.errors

                    dispatch(
                        'Alert/setAlert',
                        { message: message, errors: errors, color: 'danger' },
                        { root: true })

                    reject(error)
                })
                .finally(() => {
                    commit('setLoading', false)
                })
        })
    },
    fetchData({ commit, dispatch }, id) {
        axios.get('/api/v1/records/' + id)
            .then(response => {
                console.log(response.data.data)
                commit('setItem', response.data.data)
            })

        dispatch('fetchParkingspotsAll')
    dispatch('fetchCarsAll')
    },

    sendEmail({ commit, state, dispatch }) {
        commit('setLoading', true)
        dispatch('Alert/resetState', null, { root: true })

        return new Promise((resolve, reject) => {
            let params = new FormData();

            params.set('email',state.email)
            params.set('id', state.item.id)
            params.set('parking_id', state.item.parking_id)
            params.set('car_id', state.item.car_id)

            axios.post('/api/v1/records/sendemail/' + state.item.alegra_id, params)
                .then(response => {
                    commit('resetState')
                    resolve()
                })
                .catch(error => {
                    let message = error.response.data.message || error.message
                    let errors  = error.response.data.errors

                    dispatch(
                        'Alert/setAlert',
                        { message: message, errors: errors, color: 'danger' },
                        { root: true })

                    reject(error)
                })
                .finally(() => {

                    commit('setLoading', false)
                })
        })
    },

    fetchParkingspotsAll({ commit }) {
        axios.get('/api/v1/parking-spots')
            .then(response => {
                commit('setParkingspotsAll', response.data.data)
            })
    },
    fetchCarsAll({ commit }) {
        axios.get('/api/v1/cars')
            .then(response => {
                commit('setCarsAll', response.data.data)
            })
    },
    setParking({ commit }, value) {
        commit('setParking', value)
    },
    setEmail({ commit }, value) {
        commit('setEmail', value)
    },
    setCar({ commit }, value) {
        commit('setCar', value)
    },
    resetState({ commit }) {
        commit('resetState')
    },
    setLoading({ commit }, value) {
        commit('setLoading', value)
    }
}

const mutations = {
    setItem(state, item) {
        state.item = item
    },
    setParking(state, value) {
        state.item.parking = value
    },
    setCar(state, value) {
        state.item.car = value
    },
    setParkingspotsAll(state, value) {
        state.parkingspotsAll = value
    },
    setCarsAll(state, value) {
        state.carsAll = value
    },
    setEmail(state, value) {
        state.email = value
    },
    setLoading(state, loading) {
        state.loading = loading
        console.log(state.loading)
    },
    resetState(state) {
        state = Object.assign(state, initialState())
    }
}

export default {
    namespaced: true,
    state: initialState,
    getters,
    actions,
    mutations
}
