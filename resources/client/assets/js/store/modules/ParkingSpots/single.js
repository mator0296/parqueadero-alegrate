function initialState() {
    return {
        item: {
            id: null,
            name: null,
            current_car: null,
            current_car_id:null,
            parent: null,
        },
        newSpot: {
            id: null,
            name: null,
            current_car: null,
            current_car_id:null,
            parent: null,
        },
        child: {
            id: null,
            name: null,
            current_car: null,
            current_car_id:null,
            parent: null,
        },
        carsAll: [],
        parkingspotsAll: [],
        newCar:true,
        newCarItem : {
            license_plate: null,
            model: null
        },
        loading: false,
        sendEmail:false,
        cost:0,
        seconds:0,
        email:''
    }
}

const getters = {
    item: state => state.item,
    loading: state => state.loading,
    carsAll: state => state.carsAll,
    parkingspotsAll: state => state.parkingspotsAll,
    newCarItem: state => state.newCarItem,
    newCarState: state => state.newCar,
    cost: state => state.cost,
    newSpot: state => state.newSpot,
    child: state => state.child,
    seconds: state => state.seconds,
    sendEmail: state => state.sendEmail,
    email: state => state.email,

}

const actions = {
    storeData({ commit, state, dispatch }) {
        commit('setLoading', true)
        dispatch('Alert/resetState', null, { root: true })

        return new Promise((resolve, reject) => {
            let params = new FormData();

            for (let fieldName in state.item) {
                let fieldValue = state.item[fieldName];
                if (typeof fieldValue !== 'object') {
                    params.set(fieldName, fieldValue);
                } else {
                    if (fieldValue && typeof fieldValue[0] !== 'object') {
                        params.set(fieldName, fieldValue);
                    } else {
                        for (let index in fieldValue) {
                            params.set(fieldName + '[' + index + ']', fieldValue[index]);
                        }
                    }
                }
            }

            if (_.isEmpty(state.item.current_car)) {
                params.set('current_car_id', '')
            } else {
                params.set('current_car_id', state.item.current_car.id)

            }
            if (_.isEmpty(state.item.parent)) {
                params.set('parent_id', '')
            } else {
                params.set('parent_id', state.item.parent.id)
            }

            axios.post('/api/v1/parking-spots', params)
                .then(response => {
                    commit('resetState')
                    resolve()
                })
                .catch(error => {
                    let message = error.response.data.message || error.message
                    let errors  = error.response.data.errors

                    dispatch(
                        'Alert/setAlert',
                        { message: message, errors: errors, color: 'danger' },
                        { root: true })

                    reject(error)
                })
                .finally(() => {
                    commit('setLoading', false)
                })
        })
    },
    updateData({ commit, state, dispatch }) {
        commit('setLoading', true)
        dispatch('Alert/resetState', null, { root: true })

        return new Promise((resolve, reject) => {
            let params = new FormData();
            params.set('_method', 'PUT')

            for (let fieldName in state.item) {
                let fieldValue = state.item[fieldName];
                if (typeof fieldValue !== 'object') {
                    params.set(fieldName, fieldValue);
                } else {
                    if (fieldValue && typeof fieldValue[0] !== 'object') {
                        params.set(fieldName, fieldValue);
                    } else {
                        for (let index in fieldValue) {
                            params.set(fieldName + '[' + index + ']', fieldValue[index]);
                        }
                    }
                }
            }

            if (_.isEmpty(state.item.current_car)) {
                params.set('current_car_id', '')
            } else {
                params.set('current_car_id', state.item.current_car.id)

            }
            if (_.isEmpty(state.item.parent)) {
                params.set('parent_id', '')
            } else {
                params.set('parent_id', state.item.parent.id)

            }


            axios.post('/api/v1/parking-spots/' + state.item.id, params)
                .then(response => {
                    commit('setItem', response.data.data)
                    resolve()
                })
                .catch(error => {
                    let message = error.response.data.message || error.message
                    let errors  = error.response.data.errors

                    dispatch(
                        'Alert/setAlert',
                        { message: message, errors: errors, color: 'danger' },
                        { root: true })

                    reject(error)
                })
                .finally(() => {
                    commit('setLoading', false)
                })
        })
    },
    RegisterCarSpot({ commit, state, dispatch }) {
        commit('setLoading', true)
        dispatch('Alert/resetState', null, { root: true })

        return new Promise((resolve, reject) => {
            let params = new FormData();
            params.set('_method', 'PUT')

            for (let fieldName in state.item) {
                let fieldValue = state.item[fieldName];
                if (typeof fieldValue !== 'object') {
                    params.set(fieldName, fieldValue);
                } else {
                    if (fieldValue && typeof fieldValue[0] !== 'object') {
                        params.set(fieldName, fieldValue);
                    } else {
                        for (let index in fieldValue) {
                            params.set(fieldName + '[' + index + ']', fieldValue[index]);
                        }
                    }
                }
            }

            if (_.isEmpty(state.item.current_car)) {
                params.set('current_car_id', '')
            } else {
                params.set('current_car_id', state.item.current_car.id)

            }
            if (_.isEmpty(state.item.parent)) {
                params.set('parent_id', '')
            } else {
                params.set('parent_id', state.item.parent.id)

            }
            if (state.newCar) {

                if (_.isEmpty(state.newCarItem.license_plate)) {
                    params.set('license_plate', '')
                } else {
                    params.set('license_plate', state.newCarItem.license_plate)

                }
                if (_.isEmpty(state.newCarItem.model)) {
                    params.set('model', '')
                } else {
                    params.set('model', state.newCarItem.model)
                }

                params.set('newCar', state.newCar)
            }

            axios.post('/api/v1/parking-spots/register-car-spot/' + state.item.id, params)
                .then(response => {
                    commit('setItem', response.data.data)
                    resolve()
                })
                .catch(error => {
                    let message = error.response.data.message || error.message
                    let errors  = error.response.data.errors

                    dispatch(
                        'Alert/setAlert',
                        { message: message, errors: errors, color: 'danger' },
                        { root: true })

                    reject(error)
                })
                .finally(() => {
                    commit('setLoading', false)
                })
        })
    },
    fetchData({ commit, dispatch }, id) {
        axios.get('/api/v1/parking-spots/' + id)
            .then(response => {
                commit('setItem', response.data.data)
            })

        dispatch('fetchCarsAll')
        dispatch('fetchParkingspotsAll')
    },

    fetchDataNewCar({ commit, dispatch }, id) {
        axios.get('/api/v1/parking-spots/' + id)
            .then(response => {
                commit('setItem', response.data.data)
            })

        dispatch('fetchCarsFree')
        dispatch('fetchParkingspotsAll')
    },

    fetchDataCheckOut({commit, dispatch}, id) {
            axios.get('/api/v1/parking-spots/checkoutdata/' + id)
                .then(response => {
                    commit('setItems', response.data)
                })

            dispatch('fetchCarsAll')
            dispatch('fetchParkingspotsAll')
    },

    disableSpot({ commit, dispatch, state }) {

        commit('setLoading', true)
        dispatch('Alert/resetState', null, { root: true })

        return new Promise((resolve, reject) => {
            let params = new FormData();
            params.set('id', state.item.id)
            params.set('name', state.item.name)
            params.set('seconds', state.seconds)
            params.set('cost', state.cost)
            params.set('child_id', state.child.id ? state.child.id : 0)
            params.set('newSpot_id', state.newSpot.id ? state.newSpot.id : 0)
            if (state.sendEmail) {
                params.set('email', state.email)
            }
            console.log(state)
            params.set('_method', 'PUT')
            axios.post('/api/v1/parking-spots/checkout/' + state.item.id, params)
                .then(response => {
                    //commit('setItem', response.data.data)
                    resolve()
                })
                .catch(error => {
                    let message = error.response.data.message || error.message
                    let errors  = error.response.data.errors

                    dispatch(
                        'Alert/setAlert',
                        { message: message, errors: errors, color: 'danger' },
                        { root: true })

                    reject(error)
                })
                .finally(() => {
                    commit('setLoading', false)
                })
        })
    },

    fetchCarsAll({ commit }) {
        axios.get('/api/v1/cars')
            .then(response => {
                commit('setCarsAll', response.data.data)
            })
    },
    fetchCarsFree({ commit }) {
        axios.get('/api/v1/cars/free')
            .then(response => {
                commit('setCarsAll', response.data.data)
            })
    },
    fetchParkingspotsAll({ commit }) {
        axios.get('/api/v1/parking-spots')
            .then(response => {
                commit('setParkingspotsAll', response.data.data)
            })
    },
    setName({ commit }, value) {
        commit('setName', value)
    },
    setCurrent_car({ commit }, value) {
        commit('setCurrent_car', value)
    },
    setParent({ commit }, value) {
        commit('setParent', value)
    },
    setEmail({ commit }, value) {
        commit('setEmail', value)
    },
    resetState({ commit }) {
        commit('resetState')
    },
    getNewSpot({commit, dispatch}) {
        commit('setLoading', true)

        axios.get('/api/v1/parking-spots/new-spot-for-car')
            .then(response => {
                console.log(response.data)
                commit('setItem', response.data.data)
            })
            dispatch('fetchCarsAll')
            dispatch('fetchParkingspotsAll')
    },
    setNewCarItemLincensePlate({ commit }, value) {
        console.log(value)
        commit('setNewCarItemLincensePlate', value)
    },
    setNewCarItemModel({ commit }, value) {
        commit('setNewCarItemModel', value)
    },
    changeNewCardState({ commit }) {
        commit('changeNewCardState')
    },
    changeSendEmailState({ commit }) {
        commit('changeSendEmailState')
    },
    reculateCost({commit, state}) {
        const seconds = Math.abs((new Date().getTime()- new Date(state.item.updated_at).getTime())/1000).toFixed()
        commit('setCost', (seconds*30).toFixed(2))
        commit('setSeconds', seconds)
    }

}

const mutations = {
    setItem(state, item) {
        state.item = item
    },
    setItems(state, items) {

        state.item = items.spot
        if (!Array.isArray(items.newSpot))
            state.newSpot = items.newSpot
        if (!Array.isArray(items.child))
            state.child = items.child

            console.log(state)
    },
    setName(state, value) {
        state.item.name = value
    },
    setCurrent_car(state, value) {
        state.item.current_car = value
    },
    setParent(state, value) {
        state.item.parent = value
    },
    setCarsAll(state, value) {
        state.carsAll = value
    },
    setParkingspotsAll(state, value) {
        state.parkingspotsAll = value
    },

    setLoading(state, loading) {
        state.loading = loading
    },
    setCost(state, cost) {
        state.cost = cost
    },
    setEmail(state, email) {
        state.email = email
    },
    setSeconds(state, seconds) {
        state.seconds = seconds
    },
    setNewCarItemLincensePlate(state, value) {

        state.newCarItem.license_plate = value
        console.log(state.newCarItem)
    },
    setNewCarItemModel(state, value) {

        state.newCarItem.model = value
        console.log(state.newCarItem)
    },
    changeNewCardState(state) {
        state.newCar = !state.newCar
    },
    changeSendEmailState(state) {
        state.sendEmail = !state.sendEmail
    },
    resetState(state) {
        state = Object.assign(state, initialState())
    },


}

export default {
    namespaced: true,
    state: initialState,
    getters,
    actions,
    mutations
}
