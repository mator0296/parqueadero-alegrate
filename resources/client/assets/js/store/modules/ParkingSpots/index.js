function initialState() {
    return {
        all: [],
        relationships: {
            'current_car': 'license_plate',
            'parent': 'name',
        },
        query: {},
        loading: false
    }
}

const getRandomInt = (min, max) => {
  return Math.floor(Math.random() * (max - min)) + min;
}


const getters = {
    data: state => {
        let rows = state.all

        if (state.query.sort) {
            rows = _.orderBy(state.all, state.query.sort, state.query.order)
        }

        return rows.slice(state.query.offset, state.query.offset + state.query.limit)
    },
    total:         state => state.all.length,
    loading:       state => state.loading,
    relationships: state => state.relationships,
    dataWithoutParent: state => state.all.filter(data=>!data.parent_id),
    dataWithParent: state => state.all.filter(data=>data.parent_id),
    dataWithCar: state => state.all.filter(data=>data.current_car_id).map(data=>({...data, actualModel:data.current_car.model})),
    dataWithOutCar: state => state.all.filter(data=>!data.current_car_id),
    newSpot: state => {
        let withOutCar = state.all.filter(data=>!data.current_car_id)

        let ramdom = withOutCar[getRandomInt(0, withOutCar.length)]
        if (ramdom)
            if (ramdom.parent)
                if (!ramdom.parent.current_car_id)
                    ramdom = ramdom.parent

        console.log(ramdom)
        return ramdom ? ramdom : {};
    },


}

const actions = {
    fetchData({ commit, state }) {
        commit('setLoading', true)

        axios.get('/api/v1/parking-spots')
            .then(response => {
                commit('setAll', response.data.data.map(data=>({...data, secondsAlive: Math.abs((new Date().getTime()- new Date(data.updated_at).getTime())/1000).toFixed()})))
            })
            .catch(error => {
                message = error.response.data.message || error.message
                commit('setError', message)
                console.log(message)
            })
            .finally(() => {
                commit('setLoading', false)
            })
    },
    fetchDataTop5({ commit, state }) {
        commit('setLoading', true)

        axios.get('/api/v1/parking-spots/mosttime')
            .then(response => {
                commit('setAll', response.data.data)
            })
            .catch(error => {
                message = error.response.data.message || error.message
                commit('setError', message)
                console.log(message)
            })
            .finally(() => {
                commit('setLoading', false)
            })
    },
    destroyData({ commit, state }, id) {
        axios.delete('/api/v1/parking-spots/' + id)
            .then(response => {
                commit('setAll', state.all.filter((item) => {
                    return item.id != id
                }))
            })
            .catch(error => {
                message = error.response.data.message || error.message
                commit('setError', message)
                console.log(message)
            })
    },
    setQuery({ commit }, value) {
        commit('setQuery', purify(value))
    },
    resetState({ commit }) {
        commit('resetState')
    }
}

const mutations = {
    setAll(state, items) {
        state.all = items
    },
    setLoading(state, loading) {
        state.loading = loading
    },
    setQuery(state, query) {
        state.query = query
    },
    resetState(state) {
        state = Object.assign(state, initialState())
    }
}

export default {
    namespaced: true,
    state: initialState,
    getters,
    actions,
    mutations
}
