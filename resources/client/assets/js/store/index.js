import Vue from 'vue'
import Vuex from 'vuex'
import Alert from './modules/alert'
import ChangePassword from './modules/change_password'
import Rules from './modules/rules'
import UsersIndex from './modules/Users'
import UsersSingle from './modules/Users/single'
import RolesIndex from './modules/Roles'
import RolesSingle from './modules/Roles/single'
import PermissionsIndex from './modules/Permissions'
import PermissionsSingle from './modules/Permissions/single'
import ParkingSpotsIndex from './modules/ParkingSpots'
import ParkingSpotsSingle from './modules/ParkingSpots/single'
import CarsIndex from './modules/Cars'
import CarsSingle from './modules/Cars/single'
import RecordsIndex from './modules/Records'
import RecordsSingle from './modules/Records/single'

Vue.use(Vuex)

const debug = process.env.NODE_ENV !== 'production'

export default new Vuex.Store({
    modules: {
        Alert,
        ChangePassword,
        Rules,
        UsersIndex,
        UsersSingle,
        RolesIndex,
        RolesSingle,
        PermissionsIndex,
        PermissionsSingle,
        ParkingSpotsIndex,
        ParkingSpotsSingle,
        CarsIndex,
        CarsSingle,
        RecordsIndex,
        RecordsSingle,
    },
    strict: debug,
})
