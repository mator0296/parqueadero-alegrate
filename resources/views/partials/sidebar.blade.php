@inject('request', 'Illuminate\Http\Request')
<!-- Left side column. contains the sidebar -->
<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <ul class="sidebar-menu">

            <li>
                <a href="{{ url('/') }}">
                    <i class="fa fa-wrench"></i>
                    <span class="title">@lang('quickadmin.qa_dashboard')</span>
                </a>
            </li>

            <li class="treeview" v-if="$can('user_management_access')">
                <a href="#">
                    <i class="fa fa-users"></i>
                    <span>@lang('quickadmin.user-management.title')</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li v-if="$can('user_access')">
                        <router-link :to="{ name: 'users.index' }">
                            <i class="fa fa-user"></i>
                            <span>@lang('quickadmin.users.title')</span>
                        </router-link>
                    </li>
                    <li v-if="$can('role_access')">
                        <router-link :to="{ name: 'roles.index' }">
                            <i class="fa fa-briefcase"></i>
                            <span>@lang('quickadmin.roles.title')</span>
                        </router-link>
                    </li>
                    <li v-if="$can('permission_access')">
                        <router-link :to="{ name: 'permissions.index' }">
                            <i class="fa fa-briefcase"></i>
                            <span>@lang('quickadmin.permissions.title')</span>
                        </router-link>
                    </li>
                </ul>
            </li>
            <li class="treeview" v-if="$can('parquing_access')">
                <a href="#">
                    <i class="fa fa-home"></i>
                    <span>@lang('quickadmin.parquing.title')</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li v-if="$can('parking_spot_access')">
                        <router-link :to="{ name: 'parking_spots.general' }">
                            <i class="fa fa-car"></i>
                            <span>@lang('quickadmin.parking_general')</span>
                        </router-link>
                    </li>
                    <li v-if="$can('parking_spot_access')">
                        <router-link :to="{ name: 'parking_spots.index' }">
                            <i class="fa fa-car"></i>
                            <span>@lang('quickadmin.parking-spot.title')</span>
                        </router-link>
                    </li>
                    <li v-if="$can('car_access')">
                        <router-link :to="{ name: 'cars.index' }">
                            <i class="fa fa-car"></i>
                            <span>@lang('quickadmin.car.title')</span>
                        </router-link>
                    </li>
                </ul>
            </li>
            <li class="treeview" v-if="$can('record-gereal_access')">
                <a href="#">
                    <i class="fa fa-sort-amount-asc"></i>
                    <span>@lang('quickadmin.record-gereal.title')</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li v-if="$can('record_access')">
                        <router-link :to="{ name: 'records.index' }">
                            <i class="fa fa-book"></i>
                            <span>@lang('quickadmin.record.title')</span>
                        </router-link>
                    </li>
                    <li v-if="$can('record_access')">
                        <router-link :to="{ name: 'records.mostcar' }">
                            <i class="fa fa-book"></i>
                            <span>@lang('quickadmin.record.mostcar')</span>
                        </router-link>
                    </li>
                    <li v-if="$can('record_access')">
                        <router-link :to="{ name: 'records.mostspot' }">
                            <i class="fa fa-book"></i>
                            <span>@lang('quickadmin.record.mostspot')</span>
                        </router-link>
                    </li>
                </ul>
            </li>

            <li>
                <router-link :to="{ name: 'auth.change_password' }">
                    <i class="fa fa-key"></i>
                    <span class="title">@lang('quickadmin.qa_change_password')</span>
                </router-link>
            </li>

            <li>
                <a href="#logout" onclick="$('#logout').submit();">
                    <i class="fa fa-arrow-left"></i>
                    <span class="title">@lang('quickadmin.qa_logout')</span>
                </a>
            </li>
        </ul>
    </section>
</aside>
