<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Update1541876428ParkingSpotsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('parking_spots', function (Blueprint $table) {
            if(Schema::hasColumn('parking_spots', 'status')) {
                $table->dropColumn('status');
            }
            if(Schema::hasColumn('parking_spots', 'parent')) {
                $table->dropColumn('parent');
            }
            
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('parking_spots', function (Blueprint $table) {
                        $table->tinyInteger('status')->default('0');
                $table->integer('parent')->nullable()->unsigned();
                
        });

    }
}
