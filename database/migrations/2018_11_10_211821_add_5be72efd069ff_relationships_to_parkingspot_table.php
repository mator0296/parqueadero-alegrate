<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Add5be72efd069ffRelationshipsToParkingSpotTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('parking_spots', function(Blueprint $table) {
            if (!Schema::hasColumn('parking_spots', 'current_car_id')) {
                $table->integer('current_car_id')->unsigned()->nullable();
                $table->foreign('current_car_id', '19202_5be72accb5a28')->references('id')->on('cars')->onDelete('cascade');
                }
                if (!Schema::hasColumn('parking_spots', 'parent_id')) {
                $table->integer('parent_id')->unsigned()->nullable();
                $table->foreign('parent_id', '19202_5be72b1f8fba1')->references('id')->on('parking_spots')->onDelete('cascade');
                }
                
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('parking_spots', function(Blueprint $table) {
            
        });
    }
}
