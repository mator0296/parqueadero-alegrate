<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Add5be72fffd6ca9RelationshipsToRecordTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('records', function(Blueprint $table) {
            if (!Schema::hasColumn('records', 'parking_id')) {
                $table->integer('parking_id')->unsigned()->nullable();
                $table->foreign('parking_id', '19204_5be727d89ff49')->references('id')->on('parking_spots')->onDelete('cascade');
            }

            if (!Schema::hasColumn('records', 'parking_id_out')) {
                $table->integer('parking_id_out')->unsigned()->nullable();
                $table->foreign('parking_id_out', '19204_5be727d89ff41')->references('id')->on('parking_spots')->onDelete('cascade');
            }

            if (!Schema::hasColumn('records', 'car_id')) {
                $table->integer('car_id')->unsigned()->nullable();
                $table->foreign('car_id', '19204_5be7278aa580f')->references('id')->on('cars')->onDelete('cascade');
            }

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('records', function(Blueprint $table) {

        });
    }
}
