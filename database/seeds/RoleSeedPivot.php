<?php

use Illuminate\Database\Seeder;

class RoleSeedPivot extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $items = [

            1 => [
                'permission' => [1, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 18, 19, 20, 21, 32, 33, 34, 35, 36, 2, 3, 4, 5, 6, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 53, 54, 55],
            ],
            2 => [
                'permission' => [18, 19, 20, 35, 2, 3, 4, 5, 48, 50, 53, 54],
            ],

        ];

        foreach ($items as $id => $item) {
            $role = \App\Role::find($id);

            foreach ($item as $key => $ids) {
                $role->{$key}()->sync($ids);
            }
        }
    }
}
