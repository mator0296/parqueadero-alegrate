<?php

use Illuminate\Database\Seeder;

class ParkingspotSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $items = [

            ['id' => 1, 'name' => 'C1', 'parent_id' => null, 'current_car_id' => null],
            ['id' => 2, 'name' => 'C2', 'parent_id' => null, 'current_car_id' => null],
            ['id' => 3, 'name' => 'C3', 'parent_id' => null, 'current_car_id' => null],
            ['id' => 4, 'name' => 'C4', 'parent_id' => null, 'current_car_id' => null],
            ['id' => 5, 'name' => 'C5', 'parent_id' => null, 'current_car_id' => null],
            ['id' => 6, 'name' => 'C6', 'parent_id' => null, 'current_car_id' => null],
            ['id' => 7, 'name' => 'C7', 'parent_id' => null, 'current_car_id' => null],
            ['id' => 8, 'name' => 'C8', 'parent_id' => null, 'current_car_id' => null],
            ['id' => 9, 'name' => 'C9', 'parent_id' => null, 'current_car_id' => null],
            ['id' => 10, 'name' => 'C10', 'parent_id' => null, 'current_car_id' => null],
            ['id' => 11, 'name' => 'C11', 'parent_id' => 1, 'current_car_id' => null],
            ['id' => 12, 'name' => 'C12', 'parent_id' => 2, 'current_car_id' => null],
            ['id' => 13, 'name' => 'C13', 'parent_id' => 3, 'current_car_id' => null],
            ['id' => 14, 'name' => 'C14', 'parent_id' => 4, 'current_car_id' => null],
            ['id' => 15, 'name' => 'C15', 'parent_id' => 5, 'current_car_id' => null],
            ['id' => 16, 'name' => 'C16', 'parent_id' => 6, 'current_car_id' => null],
            ['id' => 17, 'name' => 'C17', 'parent_id' => 7, 'current_car_id' => null],
            ['id' => 18, 'name' => 'C18', 'parent_id' => 8, 'current_car_id' => null],
            ['id' => 19, 'name' => 'C19', 'parent_id' => 9, 'current_car_id' => null],
            ['id' => 20, 'name' => 'C20', 'parent_id' => 10, 'current_car_id' => null],

        ];

        foreach ($items as $item) {
            \App\ParkingSpot::create($item);
        }
    }
}
