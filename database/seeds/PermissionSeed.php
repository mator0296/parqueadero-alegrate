<?php

use Illuminate\Database\Seeder;

class PermissionSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $items = [

            ['id' => 1, 'title' => 'user_management_access',],
            ['id' => 2, 'title' => 'permission_access',],
            ['id' => 3, 'title' => 'permission_create',],
            ['id' => 4, 'title' => 'permission_edit',],
            ['id' => 5, 'title' => 'permission_view',],
            ['id' => 6, 'title' => 'permission_delete',],
            ['id' => 7, 'title' => 'role_access',],
            ['id' => 8, 'title' => 'role_create',],
            ['id' => 9, 'title' => 'role_edit',],
            ['id' => 10, 'title' => 'role_view',],
            ['id' => 11, 'title' => 'role_delete',],
            ['id' => 12, 'title' => 'user_access',],
            ['id' => 13, 'title' => 'user_create',],
            ['id' => 14, 'title' => 'user_edit',],
            ['id' => 15, 'title' => 'user_view',],
            ['id' => 16, 'title' => 'user_delete',],
            ['id' => 18, 'title' => 'contact_management_create',],
            ['id' => 19, 'title' => 'contact_management_edit',],
            ['id' => 20, 'title' => 'contact_management_view',],
            ['id' => 21, 'title' => 'contact_management_delete',],
            ['id' => 32, 'title' => 'parking_spot_access',],
            ['id' => 33, 'title' => 'parking_spot_create',],
            ['id' => 34, 'title' => 'parking_spot_edit',],
            ['id' => 35, 'title' => 'parking_spot_view',],
            ['id' => 36, 'title' => 'parking_spot_delete',],
            ['id' => 42, 'title' => 'record_access',],
            ['id' => 43, 'title' => 'record_create',],
            ['id' => 44, 'title' => 'record_edit',],
            ['id' => 45, 'title' => 'record_view',],
            ['id' => 46, 'title' => 'record_delete',],
            ['id' => 47, 'title' => 'car_access',],
            ['id' => 48, 'title' => 'car_create',],
            ['id' => 49, 'title' => 'car_edit',],
            ['id' => 50, 'title' => 'car_view',],
            ['id' => 51, 'title' => 'car_delete',],
            ['id' => 53, 'title' => 'parquing_access',],
            ['id' => 54, 'title' => 'record-gereal_access',],
            ['id' => 55, 'title' =>'parking_spot_disable_enable']

        ];

        foreach ($items as $item) {
            \App\Permission::create($item);
        }
    }
}
