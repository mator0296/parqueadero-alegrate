<?php

$factory->define(App\ParkingSpot::class, function (Faker\Generator $faker) {
    return [
        "name" => $faker->name,
        "current_car_id" => factory('App\Car')->create(),
        "parent_id" => factory('App\ParkingSpot')->create(),
    ];
});
