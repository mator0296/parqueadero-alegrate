<?php

$factory->define(App\Car::class, function (Faker\Generator $faker) {
    return [
        "license_plate" => $faker->name,
        "model" => $faker->name,
    ];
});
