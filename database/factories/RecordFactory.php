<?php

$factory->define(App\Record::class, function (Faker\Generator $faker) {
    return [
        "parking_id" => factory('App\ParkingSpot')->create(),
        "car_id" => factory('App\Car')->create(),
    ];
});
